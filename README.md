![Papyr](papyr1.jpg)


## Getting Started 

Thank you for purchasing **Papyr**, the compact Nordic nRF52840 based epaper 
display developed by Electronut Labs.

Here's how you can get started with Papyr.

## Hardware Setup

Insert a CR2477 coin cell into the holder, or connect power via the micro USB 
connector. Make sure S2 is set to the correct position - USB or BATT.

The LED should start blinking GREEN. At this point, it's advertising 
as a BLE peripheral and you can connect to it from the Electronut Labs mobile 
app. 

## Using the Mobile App 

Download the Electronut Labs mobile app from the [Android Play store](https://play.google.com/store/apps/details?id=in.electronut.app). 
(The iOS version will be released soon.)

Click on *Papyr*, and you will should find your Papyr device in the list of 
scanned BLE peripherals. Click on the Papyr entry and you will see the following 
dialog. 

There are three options on application - Draw, Select image, Trasnfer data over MQTT.

### Draw on Phone

![papyr app](app1.png)

As shown above, draw something on the screen, and hit "Send Image" when done. The 
LED on Papyr will blink BLE as the image is transferred, and in less than 30 
seconds, your image will appear on Papyr.

![papyr drawing](papyr2.jpg)

### Select Image from Device

![papyr local image](app2.jpg)

Second way to connect to papyr using application is by selecting a file already present on your phone. After selecting the option, you will get a screen that asks to select an image from your gallery. After selecting, change the threshold depending on the closeness of color to red and black. After that, just hit the "Send" button to transfer the processed image to papyr display. 

![papyr local image](papyr3.jpg)

### Transfer Image Data over MQTT

Third mode of image transfer to papyr involves transfer of image over MQTT. You need internet connection on your mobile device to connect to a broker. 

![papyr local image](mqtt_all.png)

* Select "Send Data over MQTT" on clicking the papyr device from the scanned list.
* Click MQTT Config to connect app to MQTT over Websockets. Default to app are linked to hivemq's mqtt-dashboard with "hostname : broker.mqttdashboard.com" and "port : 8000". Tap "Connect" button to connect app to mqtt broker.
* Once connected, you will get the topics to connect to. You can use either a MQTT web client, like [http://www.hivemq.com/demos/websocket-client/](http://www.hivemq.com/demos/websocket-client/) , and connect to MQTT broker mentioned above.
* On the HiveMQ web client, connect to same host. Set topic name to one mentioned on the app. You can send two types of data:
    * SVG - create a svg using [online tools](https://www.rapidtables.com/web/tools/svg-viewer-editor.html) 
    * Base64 - Use [online tool](http://b64.io/) to convert image to base64.
    * Example : 

```xml 
    <svg xmlns="http://www.w3.org/2000/svg" width="200" height="200" version="1.1">
        <g>
            <rect width="200" height="100" stroke="black" stroke-width="0" fill="red" />
            <text x="10" y="80" font-size="30" stroke="white" stroke-width="0" fill="white">Electronut</text>
            <circle cx="170" cy="50" r="10" fill="white" />
            <circle cx="170" cy="20" r="10" fill="black" />
        </g>
        <g>
            <rect y="100" width="200" height="100" stroke="black" stroke-width="0" fill="black" />
            <text x="10" y="120" stroke="white" stroke-width="0" font-size="20" fill="white">Labs</text>
            <circle cx="170" cy="150" r="10" fill="white" />
            <circle cx="170" cy="180" r="10" fill="red" />
        </g>
    </svg>

```
* After publishing content from hive's web client, check the threshold on app for both red and black color and hit "Send" to display the image on papyr display.

![papyr mqtt image](papyr4.jpg)

Note that when not connected, Papyr will go to sleep to conserve power in about a 
minute. To wake it up, just press the S1 push button.

## Programming Papyr

Papyr can be reprogrammed using SWD header. You can use our inexpensive [Bumpy]()
SWD debugger for that purpose. 

![papyr programming](papyr-prog.jpg)

You can read about the procedure [here](https://github.com/electronut/ElectronutLabs-Bumpy).

You can find several interesting applications of Papyr in the *code* directory.

If you need to revert to the default firmware, you can find the hex files under 
the top level *firmware* folder in this repository.

## Hardware Specifications

- Raytac MDBT50 module with Nordic nRF52840 BLE/802.15.4 SoC
- 1.54 inch 200x200 pixel red/black/white epaper display 
- CR2477 coin cell holder
- Micro USB (device) 
- RGB LED
- NFC (PCB) antenna
- Push button
- USB/Battery power switch
- Extra GPIOs
- SWD Programming header
- Mounting holes

## Papyr Dimensions

![papyr dims](papyr-dims.png)

## Current Consumption 

Papyr is designed for very low power applications. For the default fimrware that 
ships with Papyr, when the device is sleeping (not advertising), the current 
draw is about 6 uA. 

## Code Repository

You can find all code and design files related to Papyr at the link below:

[https://gitlab.com/electronutlabs-public/papyr/](https://gitlab.com/electronutlabs-public/papyr/)

## About Electronut Labs

**Electronut Labs** is an Embedded Systems company based in Bangalore, India. More 
information at our [website](https://electronut.in).





#include <zephyr/types.h>
#include <stddef.h>
#include <string.h>
#include <errno.h>
#include <misc/printk.h>
#include <misc/byteorder.h>
#include <zephyr.h>

#include <settings/settings.h>

#include <bluetooth/bluetooth.h>
#include <bluetooth/hci.h>
#include <bluetooth/conn.h>
#include <bluetooth/uuid.h>
#include <bluetooth/gatt.h>

#include <device.h>
#include <gpio.h>
#include <spi.h>

#include "epd1in54b.h"
#include "epdif.h"
#include "epdpaint.h"
#include "image.h"
#include "fonts.h"

EPD epd;
unsigned char frame_buffer_black_arr[EPD_WIDTH * EPD_HEIGHT / 8];
unsigned char *frame_buffer_black = frame_buffer_black_arr;
unsigned char frame_buffer_red_arr[EPD_WIDTH * EPD_HEIGHT / 8];
unsigned char *frame_buffer_red = frame_buffer_red_arr;
Paint paint_black;
Paint paint_red;

unsigned char uncompressed_red_bw_imageData[(((EPD_WIDTH * EPD_HEIGHT) / 8))*2];
unsigned char compressed_red_bw_imageData[(((EPD_WIDTH * EPD_HEIGHT) / 8) + 40)*2];

#define GREEN_LED 13
#define BLUE_LED 14
#define RED_LED 15
#define RESET_PIN 18
#define E_INK 11
#define P0_08 8

#define LEDS_ON 1

int epaper_init();
void epaper_paint_init();
void epaper_test();
void epaper_clear();

struct device * port0;

struct k_timer adv_stop_timer __attribute__((aligned(4)));
struct k_timer adv_led_timer __attribute__((aligned(4)));

// Turn off RGB LED
void leds_off()
{
	gpio_pin_write(port0, GREEN_LED, 1);
    gpio_pin_write(port0, BLUE_LED, 1);
    gpio_pin_write(port0, RED_LED, 1);
}

// Turn off epaper pins
void epaper_pins_off()
{
    gpio_pin_configure(port0, EPD_BUSY_PIN, GPIO_DIR_OUT);
	gpio_pin_configure(port0, EPD_RST_PIN, GPIO_DIR_OUT);
	gpio_pin_configure(port0, EPD_DC_PIN, GPIO_DIR_OUT);
	gpio_pin_configure(port0, EPD_SPI_CS_PIN, GPIO_DIR_OUT);
	gpio_pin_configure(port0, EPD_SPI_SCK_PIN, GPIO_DIR_OUT);
	gpio_pin_configure(port0, EPD_SPI_MOSI_PIN, GPIO_DIR_OUT);

    gpio_pin_write(port0, EPD_BUSY_PIN, 0);
	gpio_pin_write(port0, EPD_RST_PIN, 0);
	gpio_pin_write(port0, EPD_DC_PIN, 0);
	gpio_pin_write(port0, EPD_SPI_CS_PIN, 0);
	gpio_pin_write(port0, EPD_SPI_SCK_PIN, 0);
	gpio_pin_write(port0, EPD_SPI_MOSI_PIN, 0);
}

/* Custom Service Variables */
static struct bt_uuid_128 vnd_uuid = BT_UUID_INIT_128(
	0xf0, 0xde, 0xbc, 0x9a, 0x78, 0x56, 0x34, 0x12,
	0x78, 0x56, 0x34, 0x12, 0x78, 0x56, 0x34, 0x12);

static struct bt_uuid_128 vnd_enc_uuid = BT_UUID_INIT_128(
	0xf1, 0xde, 0xbc, 0x9a, 0x78, 0x56, 0x34, 0x12,
	0x78, 0x56, 0x34, 0x12, 0x78, 0x56, 0x34, 0x12);

static u8_t vnd_value[18] = {'0'};
static u8_t number = 0U;

static ssize_t read_vnd(struct bt_conn *conn, const struct bt_gatt_attr *attr,
			void *buf, u16_t len, u16_t offset)
{
	const char *value = attr->user_data;

	printk("value read : %s \n", value);

	return bt_gatt_attr_read(conn, attr, buf, len, offset, value,
				 strlen(value));
}

/*
	Uncompress a RLE buffer
	This is of little use since the size of the output buffer
	isn't known. However this can be used as the basis for ones
	own decoder.
*/
void RLEUncompress(unsigned char *output,const unsigned char *input,int length)
{
   signed char count;

   while (length > 0) {
      count = (signed char)*input++;
      if (count > 0) {                      /* replicate run */
         memset(output,*input++,count);
      } else if (count < 0) {               /* literal run */
         count = (signed char)-count;
         memcpy(output,input,count);
         input += count;
      } /* if */
      output += count;
      length -= count;
   } /* while */
}

volatile bool full_image_recieved = false;

static ssize_t write_vnd(struct bt_conn *conn, const struct bt_gatt_attr *attr,
			 const void *buf, u16_t len, u16_t offset,
			 u8_t flags)
{
	u8_t *value = attr->user_data;

	if (offset + len > sizeof(vnd_value)) {
		printk("write error\n");
		return BT_GATT_ERR(BT_ATT_ERR_INVALID_OFFSET);
	}

	memcpy(value + offset, buf, len);

	int data_length = len-2;
	static int frame_no;
	int tmp_frame_no = ((value[0]<<8) | value[1]);
	bool last_frame = false;

	static u32_t blue_led_state = 1;

	if(tmp_frame_no==0) // end frame
	{
		frame_no++;
		last_frame = true;
		printk("last frame\n");

		printk("%d : ", frame_no);

		for(int i=0; i<len; i++)
		{
			printk("%02x", value[i]);
		}
		printk("\n");

		for (int i = 0; i < data_length; i++) {
			compressed_red_bw_imageData[((frame_no-1)*16)+i] = value[i+2];
		}

		leds_off();

		#if LEDS_ON
		gpio_pin_write(port0, GREEN_LED, 0);
		#endif
		
		full_image_recieved = true;
	}
	else
	{
		frame_no = tmp_frame_no;
		printk("%d : ", frame_no);

		for(int i=0; i<len; i++)
		{
			printk("%02x", value[i]);
		}
		printk("\n");

		for (int i = 0; i < data_length; i++) {
			compressed_red_bw_imageData[((frame_no-1)*data_length)+i] = value[i+2];
		}

		leds_off();
		blue_led_state = blue_led_state ? 0:1;

		#if LEDS_ON
    	gpio_pin_write(port0, RED_LED, blue_led_state);
		#endif
	}

	return len;
}

static struct bt_gatt_ccc_cfg vnd_ccc_cfg[BT_GATT_CCC_MAX] = {};
static u8_t simulate_vnd;

static void vnd_ccc_cfg_changed(const struct bt_gatt_attr *attr, u16_t value)
{
	simulate_vnd = value == BT_GATT_CCC_NOTIFY;
}

/* Vendor Primary Service Declaration */
static struct bt_gatt_attr vnd_attrs[] = {
	/* Vendor Primary Service Declaration */
	BT_GATT_PRIMARY_SERVICE(&vnd_uuid),
	BT_GATT_CHARACTERISTIC(&vnd_enc_uuid.uuid,
			       BT_GATT_CHRC_READ | BT_GATT_CHRC_WRITE |
			       BT_GATT_CHRC_NOTIFY, BT_GATT_PERM_READ | BT_GATT_PERM_WRITE,
			       read_vnd, write_vnd, vnd_value),
	BT_GATT_CCC(vnd_ccc_cfg, vnd_ccc_cfg_changed),
};

static struct bt_gatt_service vnd_svc = BT_GATT_SERVICE(vnd_attrs);

void vnd_notification_complete_cb(struct bt_conn *conn)
{
	printk("notification complete >> value : %d\n", number);
}

static void vnd_notification_simulate(void)
{
	number++;

	bt_gatt_notify_cb(NULL, &vnd_attrs[1], &number, sizeof(number), vnd_notification_complete_cb);
	//bt_gatt_notify(NULL, &vnd_attrs[1], &number, sizeof(number));
}

static const struct bt_data ad[] = {
	BT_DATA_BYTES(BT_DATA_FLAGS, (BT_LE_AD_GENERAL | BT_LE_AD_NO_BREDR)),
};

static void connected(struct bt_conn *conn, u8_t err)
{
	if (err) {
		printk("Connection failed (err %u)\n", err);
	} else {
		printk("Connected\n");

		k_timer_stop(&adv_stop_timer);
		k_timer_stop(&adv_led_timer);

		leds_off();

		#if LEDS_ON
    	gpio_pin_write(port0, GREEN_LED, 0);
		#endif
	}
}

static void disconnected(struct bt_conn *conn, u8_t reason)
{
	printk("Disconnected (reason %u)\n", reason);
	leds_off();

	gpio_pin_write(port0, RED_LED, 0);
	k_sleep(500);
	gpio_pin_write(port0, RED_LED, 1);

	k_timer_start(&adv_stop_timer, K_MSEC(60000), 0);
	k_timer_start(&adv_led_timer, K_MSEC(300), K_MSEC(300));
}

static struct bt_conn_cb conn_callbacks = {
	.connected = connected,
	.disconnected = disconnected,
};

static void bt_ready(int err)
{
	if (err) {
		printk("Bluetooth init failed (err %d)\n", err);
		return;
	}

	printk("Bluetooth initialized\n");

	bt_gatt_service_register(&vnd_svc);

	if (IS_ENABLED(CONFIG_SETTINGS)) {
		settings_load();
	}

	err = bt_le_adv_start(BT_LE_ADV_CONN_NAME, ad, ARRAY_SIZE(ad), NULL, 0);
	if (err) {
		printk("Advertising failed to start (err %d)\n", err);
		return;
	}

	printk("Advertising successfully started\n");

	k_timer_start(&adv_stop_timer, K_MSEC(60000), 0);
	k_timer_start(&adv_led_timer, K_MSEC(300), K_MSEC(300));
}

/**************************EPAPER DISPLAY**************************/
int epaper_init()
{
    epaper_GPIO_Init();

    return EPD_Init(&epd);
}

void epaper_paint_init()
{
    Paint_Init(&paint_black, frame_buffer_black, epd.width, epd.height);
    Paint_Init(&paint_red, frame_buffer_red, epd.width, epd.height);
    Paint_Clear(&paint_black, UNCOLORED);
    Paint_Clear(&paint_red, UNCOLORED);
}

void epaper_test()
{
	Paint_SetRotate(&paint_red, 3);
    Paint_DrawStringAt(&paint_red, 23, 40, "p a p y r", &Font24, COLORED);
    Paint_DrawBitmap(&paint_black, 100, 0, bwData, image_width, image_height, COLORED);

    EPD_DisplayFrame(&epd, frame_buffer_black, frame_buffer_red);

	volatile u32_t flag = 1;

	// set flag in uicr for not displaying default image after every reset
	NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Wen << NVMC_CONFIG_WEN_Pos;
	while (NRF_NVMC->READY == NVMC_READY_READY_Busy){}
	*(uint32_t *)0x10001080 = flag;
	NRF_NVMC->CONFIG = NVMC_CONFIG_WEN_Ren << NVMC_CONFIG_WEN_Pos;
	while (NRF_NVMC->READY == NVMC_READY_READY_Busy){}
}

void epaper_clear()
{
	epaper_paint_init();
	EPD_DisplayFrame(&epd, frame_buffer_black, frame_buffer_red);
}
/**************************EPAPER DISPLAY**************************/

typedef enum _ble_adv_state
{
	eBLEAdvStart = 0,
	eBLEAdvStop = 1,
	eBLEAdvIdle
} ble_adv_state;

volatile ble_adv_state gAdvState = eBLEAdvIdle;

void ble_adv_stop()
{
    printk("\nTimer expired\n");
	gAdvState = eBLEAdvStop;
}

void flash_adv_led()
{
	static u32_t led_state = 0;

	led_state = (led_state==0) ? 1:0;
	// printk("\nled toggle > state %d\n", led_state);
	gpio_pin_write(port0, GREEN_LED, led_state);
}

void main(void)
{
	printk("Starting...");

	k_timer_init(&adv_stop_timer, ble_adv_stop, NULL);
	k_timer_init(&adv_led_timer, flash_adv_led, NULL);

	int err;

	err = bt_enable(bt_ready);
	if (err) {
		printk("Bluetooth init failed (err %d)\n", err);
		return;
	}

	bt_conn_cb_register(&conn_callbacks);

	port0 = device_get_binding("GPIO_0");

	/* Set LED pin as output */
    gpio_pin_configure(port0, GREEN_LED, GPIO_DIR_OUT);
	gpio_pin_configure(port0, BLUE_LED, GPIO_DIR_OUT);
    gpio_pin_configure(port0, RED_LED, GPIO_DIR_OUT);
	leds_off();
    // turn off epaper display MOSFET
    gpio_pin_configure(port0, E_INK, GPIO_DIR_OUT);
	gpio_pin_write(port0, E_INK, 1);
	epaper_pins_off();

	k_sleep(200);

	u32_t flag;
    memcpy(&flag, (uint8_t *)0x10001080, sizeof(flag));
	printk("\n%d\n", flag);

	gpio_pin_configure(port0, P0_08, GPIO_DIR_IN | GPIO_PUD_PULL_DOWN);
	u32_t p0_08_state;
	gpio_pin_read(port0, P0_08, &p0_08_state);
	printk("\np0_08_state: %d\n", p0_08_state);

	if((flag != 1) || (p0_08_state == 1))
	{
		printk("\nDisplaying default image...\n");
		// turn on epaper display MOSFET
		gpio_pin_write(port0, E_INK, 0);

		// init epaper display
		epaper_init();

		epaper_paint_init();
		epaper_test();
		gpio_pin_write(port0, E_INK, 1);
		epaper_pins_off();
		printk("done.");
	}

	while (1) {
		k_sleep(MSEC_PER_SEC);
		
		if(gAdvState == eBLEAdvStop)
		{
			int err = bt_le_adv_stop();
			if (err) {
				printk("Failed to stop advertising(err %d)\n", err);
				return;
			}
			else
			{
				printk("Stopped advertising\n");
				k_timer_stop(&adv_led_timer);
				leds_off();

				// epaper off
				gpio_pin_write(port0, E_INK, 1);
				epaper_pins_off();
			}

			gAdvState = eBLEAdvIdle;
		}

		if(full_image_recieved)
		{
			printk("painting...\n");
			RLEUncompress(uncompressed_red_bw_imageData, compressed_red_bw_imageData, sizeof(uncompressed_red_bw_imageData));

			// epaper_init();
			gpio_pin_write(port0, E_INK, 1);
			epaper_pins_off();
			k_sleep(300);
			gpio_pin_write(port0, E_INK, 0);		

			epaper_init();
			epaper_paint_init();
			// epaper_test();
			
			Paint_Clear(&paint_black, UNCOLORED);
			Paint_Clear(&paint_red, UNCOLORED);

			Paint_DrawBitmap(&paint_black, 0, 0, uncompressed_red_bw_imageData, EPD_WIDTH, EPD_HEIGHT, COLORED);
			Paint_DrawBitmap(&paint_red, 0, 0, (uncompressed_red_bw_imageData+5000), EPD_WIDTH, EPD_HEIGHT, COLORED);
			EPD_DisplayFrame(&epd, frame_buffer_black, frame_buffer_red);

			leds_off();

			#if LEDS_ON
			gpio_pin_write(port0, GREEN_LED, 0);
			#endif

			epaper_pins_off();
			gpio_pin_write(port0, E_INK, 1);
			full_image_recieved = false;
		}
	}
}
